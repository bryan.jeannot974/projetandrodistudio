package com.ProjetAndroidStudio.Musicfy.listener;

import com.ProjetAndroidStudio.Musicfy.model.Artist;

public interface ArtistSelectListener {
    void selectedArtist(Artist artist);
}
