package com.ProjetAndroidStudio.Musicfy.listener;

import android.content.Context;

import com.ProjetAndroidStudio.Musicfy.model.Music;

public interface PlayListListener {
    void option(Context context, Music music);
}
